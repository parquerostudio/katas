package com.parqueros.ctmapp.app.utils

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ServiceGenerator {

    private const val BASE_URL = "http://www.loyalbooks.com/"
    private val retrofitBuilder = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())

    fun <S> createService(serviceClass: Class<S>): S =
        retrofitBuilder
            .client(OkHttpClient.Builder().build())
            .build()
            .create(serviceClass)

}
