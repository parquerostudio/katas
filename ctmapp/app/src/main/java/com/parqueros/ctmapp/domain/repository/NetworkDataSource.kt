package com.parqueros.ctmapp.domain.repository

import io.reactivex.Single

interface NetworkDataSource {

    fun getBook(): Single<String>

}
