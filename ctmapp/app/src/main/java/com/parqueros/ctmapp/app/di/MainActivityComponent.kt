package com.parqueros.ctmapp.app.di

import com.parqueros.ctmapp.presentation.main.MainActivity
import dagger.Subcomponent

@Subcomponent(
    modules = [
        MainActivityModule::class
    ]
)
interface MainActivityComponent {
    fun inject(activity: MainActivity)
}
