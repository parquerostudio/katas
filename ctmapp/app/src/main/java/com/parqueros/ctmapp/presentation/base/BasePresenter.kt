package com.parqueros.ctmapp.presentation.base

interface BasePresenter<V : BasePresenter.BaseView> {

    fun getView(): V?

    fun attachView(view: V)

    fun detachView()

    interface BaseView

}
