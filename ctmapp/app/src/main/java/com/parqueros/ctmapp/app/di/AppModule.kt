package com.parqueros.ctmapp.app.di

import com.parqueros.ctmapp.app.utils.Schedulers
import com.parqueros.ctmapp.app.utils.TasksSchedulers
import com.parqueros.ctmapp.data.utils.WordCounter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideRxSchedulers(): TasksSchedulers = Schedulers()

    @Provides
    @Singleton
    fun provideWordCounter(): WordCounter = WordCounter()

}
