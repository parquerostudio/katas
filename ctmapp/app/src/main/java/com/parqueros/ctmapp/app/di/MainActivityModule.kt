package com.parqueros.ctmapp.app.di

import com.parqueros.ctmapp.app.utils.Schedulers
import com.parqueros.ctmapp.domain.repository.BookRepository
import com.parqueros.ctmapp.presentation.main.MainActivityPresenter
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    fun provideMainActivityPresenter(
        schedulers: Schedulers,
        repository: BookRepository
    ): MainActivityPresenter = MainActivityPresenter(schedulers, repository)
}
