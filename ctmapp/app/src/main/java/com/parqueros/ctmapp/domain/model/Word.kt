package com.parqueros.ctmapp.domain.model

data class Word(
    val word: String,
    var count: Int = 1
)
