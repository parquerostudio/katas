package com.parqueros.ctmapp.data.utils

object TextProcessorHelper {

    @Deprecated("Use addText in [WordCounter] instead")
    fun getWordListWithCount(text: String): Map<String, Int> {
        val wordsCount = mutableMapOf<String, Int>()
        val wordList = text.replace("[^A-Za-z0-9 ]".toRegex(), " ").split("\\s+".toRegex())
        wordList.forEach {
            if (!it.isBlank()) {
                val word = it.toLowerCase()
                if (wordsCount[word] == null) {
                    wordsCount[word] = 1
                } else {
                    val currentCount = wordsCount[word] ?: 0
                    wordsCount[word] = currentCount + 1
                }
            }
        }
        return wordsCount
    }

}
