package com.parqueros.ctmapp.app.di

import com.parqueros.ctmapp.data.BookRepositoryImpl
import com.parqueros.ctmapp.data.network.NetworkDataSourceImpl
import com.parqueros.ctmapp.domain.repository.BookRepository
import com.parqueros.ctmapp.domain.repository.NetworkDataSource
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun provideNetworkDataSource(networkDataSource: NetworkDataSourceImpl): NetworkDataSource

    @Binds
    @Singleton
    abstract fun provideRepository(repositoryImpl: BookRepositoryImpl): BookRepository

}
