package com.parqueros.ctmapp.data.network

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Streaming

interface FileDownloadService {

    @Streaming
    @GET("download/text/Railway-Children-by-E-Nesbit.txt")
    fun downloadBook(): Call<ResponseBody>

}
