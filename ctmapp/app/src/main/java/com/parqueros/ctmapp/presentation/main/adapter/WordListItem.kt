package com.parqueros.ctmapp.presentation.main.adapter

import com.parqueros.ctmapp.domain.model.Word

sealed class WordListItem {

    object Header : WordListItem()

    class WordItem(val word: Word, val isPrime: Boolean) : WordListItem()

}
