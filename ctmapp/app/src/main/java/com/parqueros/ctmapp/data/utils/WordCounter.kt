package com.parqueros.ctmapp.data.utils

import com.parqueros.ctmapp.domain.model.Word

class WordCounter {

    private val wordCount = mutableListOf<Word>()

    fun getWordList(): List<Word> = wordCount

    fun addText(text: String) {
        val wordList = text.replace("[^A-Za-z0-9 ]".toRegex(), " ").split("\\s+".toRegex())
        wordList.forEach {
            val word = it.toLowerCase()
            addWord(word)
        }

    }

    private fun addWord(word: String) {
        if (word.isBlank()) {
            return
        }
        wordCount.firstOrNull { it.word == word }?.let { existingWordCount ->
            existingWordCount.count++
        } ?: wordCount.add(Word(word))
    }

}