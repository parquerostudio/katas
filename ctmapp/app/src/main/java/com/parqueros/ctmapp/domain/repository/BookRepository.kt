package com.parqueros.ctmapp.domain.repository

import com.parqueros.ctmapp.domain.model.Word
import io.reactivex.Single

interface BookRepository {

    fun getBookWords(): Single<List<Word>>

}
