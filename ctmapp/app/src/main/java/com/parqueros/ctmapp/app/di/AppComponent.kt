package com.parqueros.ctmapp.app.di

import android.app.Application
import com.parqueros.ctmapp.app.CtmApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ActivityBuilder::class,
        NetworkModule::class,
        RepositoryModule::class
    ]
)
interface AppComponent : AndroidInjector<CtmApplication> {

    fun inject(application: Application)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: CtmApplication): Builder

        fun build(): AppComponent
    }

}
