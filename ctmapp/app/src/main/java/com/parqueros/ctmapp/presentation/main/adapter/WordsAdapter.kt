package com.parqueros.ctmapp.presentation.main.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.parqueros.ctmapp.R
import com.parqueros.ctmapp.app.utils.inflate

private const val HEADER = 0
private const val WORD_ROW = 1

class WordsAdapter : RecyclerView.Adapter<WordListItemViewHolder>() {

    private val wordListToShow = mutableListOf<WordListItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        when (viewType) {
            HEADER -> WordListItemViewHolder.HeaderViewHolder(parent.inflate(R.layout.item_list_header))
            else -> WordListItemViewHolder.WordListViewHolder(
                parent.inflate(R.layout.item_list_word)
            )
        }

    override fun onBindViewHolder(holder: WordListItemViewHolder, position: Int) {
        holder.bind(wordListToShow[position])
    }

    override fun getItemCount() = wordListToShow.size

    override fun getItemViewType(position: Int) =
        when (wordListToShow[position]) {
            is WordListItem.Header -> HEADER
            is WordListItem.WordItem -> WORD_ROW
        }

    fun setWordList(wordList: List<WordListItem>) {
        wordListToShow.clear()
        wordListToShow.addAll(wordList)
        notifyDataSetChanged()
    }

}
