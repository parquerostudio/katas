package com.parqueros.ctmapp.app.di

import com.parqueros.ctmapp.app.utils.ServiceGenerator
import com.parqueros.ctmapp.data.network.FileDownloadService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideDownloadService(): FileDownloadService =
        ServiceGenerator.createService(FileDownloadService::class.java)

}
