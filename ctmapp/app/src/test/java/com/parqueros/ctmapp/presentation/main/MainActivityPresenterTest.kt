package com.parqueros.ctmapp.presentation.main

import com.parqueros.ctmapp.domain.model.Word
import com.parqueros.ctmapp.domain.repository.BookRepository
import com.parqueros.ctmapp.presentation.main.adapter.WordListItem
import com.parqueros.ctmapp.testutils.MockSchedulers
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class MainActivityPresenterTest {

    private val schedulers = MockSchedulers
    private val repository = mock<BookRepository>()
    private val view = mock<MainActivityPresenter.MainActivityView>()

    private val presenter = MainActivityPresenter(schedulers, repository)

    @Before
    fun setUp() {
        presenter.attachView(view)
    }

    @After
    fun tearDown() {
        presenter.detachView()
    }

    @Test
    fun `onViewInitialized calls to check permissions`() {
        presenter.onViewInitialized()

        verify(view, times(1)).checkPermissions()
        verifyNoMoreInteractions(repository, view)
    }

    @Test
    fun `onPermissionsNotGranted proceeds to request permissions`() {
        presenter.onPermissionsNotGranted()

        verify(view, times(1)).requestPermissions()
        verifyNoMoreInteractions(view)
    }

    @Test
    fun `onPermissionsGranted when error occurs show the error on the view`() {
        val errorMessage = "some error"
        whenever(repository.getBookWords()).thenReturn(Single.error(Throwable(errorMessage)))

        presenter.onPermissionsGranted()

        verify(view, times(1)).showLoading()
        verify(view, times(1)).hideLoading()
        verify(view, times(1)).showError(errorMessage)
        verifyNoMoreInteractions(view)
    }

    @Test
    fun `onPermissionsGranted shows loading while retrieves the book and process the text`() {
        whenever(repository.getBookWords()).thenReturn(Single.just(emptyList()))
        presenter.onPermissionsGranted()

        verify(view, times(1)).showLoading()
        verify(view, times(1)).hideLoading()
        verify(view, times(1)).showWordList(any())

        verifyNoMoreInteractions(view)
    }

    @Test
    fun `onPermissionsGranted retrieves the words of the book from the repository and process the content to show the list of words`() {
        // given
        val expectedWords = listOf(Word("a", 1), Word("b", 2))
        whenever(repository.getBookWords()).thenReturn(Single.just(expectedWords))

        // when
        presenter.onPermissionsGranted()

        // then
        verify(repository).getBookWords()
        verify(view, times(1)).showLoading()
        verify(view, times(1)).hideLoading()

        argumentCaptor<List<WordListItem>>().apply {
            verify(view, times(1)).showWordList(capture())

            assertEquals(3, firstValue.size)
            assertTrue(firstValue[0] is WordListItem.Header)
            assertTrue(firstValue[1] is WordListItem.WordItem)
            assertEquals("a", (firstValue[1] as WordListItem.WordItem).word.word)
            assertEquals(1, (firstValue[1] as WordListItem.WordItem).word.count)
            assertFalse((firstValue[1] as WordListItem.WordItem).isPrime)
            assertEquals("b", (firstValue[2] as WordListItem.WordItem).word.word)
            assertEquals(2, (firstValue[2] as WordListItem.WordItem).word.count)
            assertTrue((firstValue[2] as WordListItem.WordItem).isPrime)
        }

        verifyNoMoreInteractions(repository, view)
    }

    @Test
    fun `onPermissionsGranted show the list of words sorted by alphabetical order`() {
        // given
        val expectedWords = listOf(Word("b", 1), Word("a", 2))
        whenever(repository.getBookWords()).thenReturn(Single.just(expectedWords))

        // when
        presenter.onPermissionsGranted()

        // then
        verify(repository).getBookWords()
        verify(view, times(1)).showLoading()
        verify(view, times(1)).hideLoading()

        argumentCaptor<List<WordListItem>>().apply {
            verify(view, times(1)).showWordList(capture())

            assertEquals(3, firstValue.size)
            assertTrue(firstValue[0] is WordListItem.Header)
            assertTrue(firstValue[1] is WordListItem.WordItem)
            assertEquals("a", (firstValue[1] as WordListItem.WordItem).word.word)
            assertEquals(2, (firstValue[1] as WordListItem.WordItem).word.count)
            assertTrue((firstValue[1] as WordListItem.WordItem).isPrime)
            assertEquals("b", (firstValue[2] as WordListItem.WordItem).word.word)
            assertEquals(1, (firstValue[2] as WordListItem.WordItem).word.count)
            assertFalse((firstValue[2] as WordListItem.WordItem).isPrime)
        }

        verifyNoMoreInteractions(repository, view)
    }

}
