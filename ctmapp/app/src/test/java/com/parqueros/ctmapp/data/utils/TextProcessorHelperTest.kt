package com.parqueros.ctmapp.data.utils

import org.junit.Assert.assertEquals
import org.junit.Test

class TextProcessorHelperTest {

    private val textWithWordsRepeated = "Text with words repeated in the text  . "

    private val processor = TextProcessorHelper

    @Test
    fun `on getWordListWithCount returns the list of words with times appearing in a given text`() {
        val wordsCount = processor.getWordListWithCount(textWithWordsRepeated)

        assertEquals(2, wordsCount["text"])
        assertEquals(1, wordsCount["with"])
        assertEquals(1, wordsCount["words"])
        assertEquals(1, wordsCount["repeated"])
        assertEquals(1, wordsCount["in"])
        assertEquals(1, wordsCount["the"])
    }

}