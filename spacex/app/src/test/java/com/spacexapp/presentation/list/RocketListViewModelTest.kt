package com.spacexapp.presentation.list

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.spacexapp.MockSchedulers
import com.spacexapp.app.utils.ErrorHandler
import com.spacexapp.app.utils.SharedPreferencesHandler
import com.spacexapp.domain.model.Rocket
import com.spacexapp.domain.repository.SpaceXRepository
import com.spacexapp.presentation.navigation.SpaceXNavigator
import io.reactivex.Single
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test

class RocketListViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val schedulers = MockSchedulers
    private val errorHandler = mock<ErrorHandler>()
    private val repository = mock<SpaceXRepository>()
    private val sharedPreferencesHandler = mock<SharedPreferencesHandler>()
    private val navigator = mock<SpaceXNavigator>()

    @Test
    fun `on init view model should show welcome dialog and not load rockets if welcome dialog was not shown`() {
        // Given
        whenever(sharedPreferencesHandler.isAlreadyShownWelcomeDialog).thenReturn(false)

        // When
        val viewModel = RocketListViewModel(
            schedulers,
            errorHandler,
            repository,
            sharedPreferencesHandler,
            navigator
        )

        // Then
        assertTrue(viewModel.getViewState().value is ViewState.Welcome)
        verify(sharedPreferencesHandler, times(1)).isAlreadyShownWelcomeDialog

        verifyNoMoreInteractions(errorHandler, repository, sharedPreferencesHandler, navigator)
    }

    @Test
    fun `on init view model should load rocket data if welcome dialog was already shown`() {
        // Given
        whenever(sharedPreferencesHandler.isAlreadyShownWelcomeDialog).thenReturn(true)
        whenever(repository.getRockets()).thenReturn(Single.just(emptyList()))

        // When
        val viewModel = RocketListViewModel(
            schedulers,
            errorHandler,
            repository,
            sharedPreferencesHandler,
            navigator
        )

        // Then
        assertTrue(viewModel.getViewState().value is ViewState.Done)
        verify(sharedPreferencesHandler, times(1)).isAlreadyShownWelcomeDialog
        verify(repository, times(1)).getRockets()

        verifyNoMoreInteractions(errorHandler, repository, sharedPreferencesHandler, navigator)
    }

    @Test
    fun `on onRocketClicked should navigate to details`() {
        // Given
        whenever(sharedPreferencesHandler.isAlreadyShownWelcomeDialog).thenReturn(true)
        whenever(repository.getRockets()).thenReturn(Single.just(emptyList()))
        val rocket = Rocket("", "", "", "")

        val viewModel = RocketListViewModel(
            schedulers,
            errorHandler,
            repository,
            sharedPreferencesHandler,
            navigator
        )

        // When
        viewModel.onRocketClicked(rocket)

        // Then
        verify(navigator, times(1)).triggerNavigationToDetails(rocket)
    }

}