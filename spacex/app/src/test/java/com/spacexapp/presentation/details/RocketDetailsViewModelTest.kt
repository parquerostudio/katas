package com.spacexapp.presentation.details

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.spacexapp.MockSchedulers
import com.spacexapp.app.utils.ErrorHandler
import com.spacexapp.domain.model.Launch
import com.spacexapp.domain.model.Rocket
import com.spacexapp.domain.repository.SpaceXRepository
import com.spacexapp.presentation.details.adapter.RocketDetailsListItem
import io.reactivex.Single
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import java.util.Date


class RocketDetailsViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val repository = mock<SpaceXRepository>()
    private val errorHandler = mock<ErrorHandler>()
    private val schedulers = MockSchedulers

    private val viewModel = RocketDetailsViewModel(schedulers, errorHandler, repository)
    private val sampleRocket = Rocket("", "", "", "")

    @After
    fun tearDown() {
        verifyNoMoreInteractions(errorHandler, repository)
    }

    @Test
    fun `on loadRocketDetails should shows error when repository throws an error`() {
        // Given
        val error = Throwable()
        whenever(repository.getLaunches()).thenReturn(Single.error(error))

        // When
        viewModel.loadRocketDetails(sampleRocket)

        // Then
        assertTrue(viewModel.getViewState().value is DetailsViewState.Error)
        verify(repository, times(1)).getLaunches()
        verify(errorHandler, times(1)).handleNetworkResponseError(any(), eq(error))
    }

    @Test
    fun `on loadRocketDetails should show empty state when success but the list comes empty`() {
        // Given
        val expectedRocketDescription = "the description"
        val expectedRocketId = "rocket_id"
        val rocket =
            sampleRocket.copy(id = expectedRocketId, description = expectedRocketDescription)
        val expectedMissionName = "the mission name"
        val expectedFlightNumber = 123
        val expectedYearLaunch = 2020
        val expectedDate = Date()
        val expectedLaunchResult = false
        val expectedMissionImage = "an image path"
        val expectedLaunch = Launch(
            expectedRocketId,
            expectedMissionName,
            expectedFlightNumber,
            expectedYearLaunch,
            expectedDate,
            expectedLaunchResult,
            expectedMissionImage
        )
        val expectedLaunchesResponse =
            listOf(expectedLaunch, expectedLaunch.copy(rocketId = "another rocket id"))
        whenever(repository.getLaunches()).thenReturn(Single.just(expectedLaunchesResponse))

        // When
        viewModel.loadRocketDetails(rocket)

        // Then
        verify(repository, times(1)).getLaunches()
        assertTrue(viewModel.getViewState().value is DetailsViewState.Done)
        with(viewModel.getViewState().value as DetailsViewState.Done) {
            assertEquals(4, rocketDetails.size)
            assertTrue(rocketDetails[0] is RocketDetailsListItem.LaunchesGraphic)
            assertEquals(expectedRocketDescription, (rocketDetails[1] as RocketDetailsListItem.RocketDescription).description)

            assertEquals(expectedYearLaunch, (rocketDetails[2] as RocketDetailsListItem.RocketLaunches.YearOfLaunch).year)
            with(rocketDetails[3] as RocketDetailsListItem.RocketLaunches.Launch) {
                assertEquals(expectedMissionName, missionName)
                assertEquals(expectedFlightNumber, flightNumber)
                assertEquals(expectedDate, date)
                assertEquals(expectedLaunchResult, wasSuccessful)
                assertEquals(expectedMissionImage, imageUrl)
            }
        }
    }

}