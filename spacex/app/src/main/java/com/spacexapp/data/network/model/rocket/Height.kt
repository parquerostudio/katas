package com.spacexapp.data.network.model.rocket

import com.google.gson.annotations.SerializedName

data class Height(
    @SerializedName("meters") val meters: Double,
    @SerializedName("feet") val feet: Double
)
