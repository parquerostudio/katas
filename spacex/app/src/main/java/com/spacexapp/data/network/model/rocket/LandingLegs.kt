package com.spacexapp.data.network.model.rocket

import com.google.gson.annotations.SerializedName

data class LandingLegs(
    @SerializedName("number") val number: Int,
    @SerializedName("material") val material: String
)
