package com.spacexapp.data.network.model.rocket

import com.google.gson.annotations.SerializedName

data class SecondStage(
    @SerializedName("reusable") val reusable: Boolean,
    @SerializedName("engines") val engines: Int,
    @SerializedName("fuel_amount_tons") val fuelAmountTons: Double,
    @SerializedName("burn_time_sec") val burnTimeSec: Int,
    @SerializedName("thrust") val thrust: Thrust,
    @SerializedName("payloads") val payloads: Payloads
)
