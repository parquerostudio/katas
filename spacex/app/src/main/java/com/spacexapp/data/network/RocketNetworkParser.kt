package com.spacexapp.data.network

import com.spacexapp.data.network.model.launch.NetworkLaunch
import com.spacexapp.data.network.model.rocket.NetworkRocket
import com.spacexapp.domain.model.Launch
import com.spacexapp.domain.model.Rocket
import java.text.SimpleDateFormat
import java.util.Date
import java.util.TimeZone

object RocketNetworkParser {

    fun parseRocketsResponse(rocketsResponse: List<NetworkRocket>): List<Rocket> {
        return rocketsResponse.map { it.toDomainModel() }
    }

    fun parseLaunchesResponse(launchesResponse: List<NetworkLaunch>): List<Launch> {
        return launchesResponse.map { it.toDomainModel() }
    }

    private fun NetworkRocket.toDomainModel() =
        Rocket(rocketId, rocketName, country, description, engines.number)

    private fun NetworkLaunch.toDomainModel() = Launch(
        rocket.rocketId,
        missionName,
        flightNumber,
        launchYear,
        parseUtcDate(launchDateUtc),
        launchSuccess,
        links.missionPatchImage
    )

    private fun parseUtcDate(launchDateUtc: String): Date {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").apply {
            timeZone = TimeZone.getTimeZone("UTC")
        }
        return dateFormat.parse(launchDateUtc)
    }

}
