package com.spacexapp.data.network.model.rocket

import com.google.gson.annotations.SerializedName

data class CompositeFairing(
    @SerializedName("height") val height: Height,
    @SerializedName("diameter") val diameter: Diameter
)

