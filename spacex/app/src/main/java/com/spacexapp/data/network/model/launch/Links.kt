package com.spacexapp.data.network.model.launch

import com.google.gson.annotations.SerializedName

data class Links(
    @SerializedName("mission_patch") val missionPatchImage: String?
)
