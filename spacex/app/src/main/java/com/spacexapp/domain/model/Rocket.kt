package com.spacexapp.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Rocket(
    val id: String,
    val name: String,
    val country: String,
    val description: String,
    val enginesCount: Int = 0
) : Parcelable
