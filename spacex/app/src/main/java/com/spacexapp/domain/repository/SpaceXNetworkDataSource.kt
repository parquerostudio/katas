package com.spacexapp.domain.repository

import com.spacexapp.domain.model.Launch
import com.spacexapp.domain.model.Rocket
import io.reactivex.Single

interface SpaceXNetworkDataSource {

    fun getRockets(): Single<List<Rocket>>

    fun getLaunches(): Single<List<Launch>>

}
