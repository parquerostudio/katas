package com.spacexapp.domain.repository

import com.spacexapp.domain.model.Launch
import com.spacexapp.domain.model.Rocket
import io.reactivex.Single

interface SpaceXRepository {

    fun getRefreshedRockets(): Single<List<Rocket>>

    fun getRockets(): Single<List<Rocket>>

    fun getLaunches(): Single<List<Launch>>

}
