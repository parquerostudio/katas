package com.spacexapp.presentation.details.adapter

import java.util.Date

sealed class RocketDetailsListItem {

    data class LaunchesGraphic(val launchesPerYear: List<Pair<Int, Int>>) : RocketDetailsListItem()

    data class RocketDescription(val description: String) : RocketDetailsListItem()

    sealed class RocketLaunches : RocketDetailsListItem() {

        data class YearOfLaunch(val year: Int) : RocketLaunches()

        data class Launch(
            val missionName: String,
            val flightNumber: Int,
            val date: Date,
            val wasSuccessful: Boolean?,
            val imageUrl: String?
        ) : RocketLaunches()

    }

}
