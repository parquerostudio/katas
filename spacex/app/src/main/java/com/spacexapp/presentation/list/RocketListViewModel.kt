package com.spacexapp.presentation.list

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.spacexapp.app.utils.ErrorHandler
import com.spacexapp.app.utils.SharedPreferencesHandler
import com.spacexapp.app.utils.TasksSchedulers
import com.spacexapp.domain.model.Rocket
import com.spacexapp.domain.repository.SpaceXRepository
import com.spacexapp.presentation.list.adapter.RocketListItem
import com.spacexapp.presentation.navigation.SpaceXNavigator
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RocketListViewModel
@Inject constructor(
    private val schedulers: TasksSchedulers,
    private val errorHandler: ErrorHandler,
    private val repository: SpaceXRepository,
    private val sharedPreferencesHandler: SharedPreferencesHandler,
    private val navigator: SpaceXNavigator
) : ViewModel() {

    private val viewState = MutableLiveData<ViewState>()
    private val disposables = CompositeDisposable()

    init {
        if (sharedPreferencesHandler.isAlreadyShownWelcomeDialog) {
            loadRockets()
        } else {
            viewState.value = ViewState.Welcome
        }
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    fun getViewState(): LiveData<ViewState> = viewState

    fun getNavigatorTriggerToDetails(): LiveData<Rocket> = navigator.navigateToDetailsTrigger

    fun welcomeDialogShown() {
        sharedPreferencesHandler.isAlreadyShownWelcomeDialog = true
        loadRockets()
    }

    fun onRocketClicked(rocket: Rocket) {
        navigator.triggerNavigationToDetails(rocket)
    }

    fun reLoadRockets() {
        disposables += repository.getRefreshedRockets()
            .getLoadRocketDisposableConfiguration(true)
    }

    private fun loadRockets() {
        disposables += repository.getRockets()
            .getLoadRocketDisposableConfiguration(false)
    }

    private fun handleGetRocketsSuccess(rockets: List<Rocket>) {
        viewState.postValue(ViewState.Done(normalizeRocketData(rockets)))
    }

    private fun handleGetRocketsError(error: Throwable) {
        errorHandler.handleNetworkResponseError(javaClass.simpleName, error)
        viewState.postValue(ViewState.Error(error))
    }

    private fun normalizeRocketData(rockets: List<Rocket>): List<RocketListItem> =
        listOf(RocketListItem.Header) +
            rockets.map { rocket ->
                RocketListItem.RocketItem(rocket)
            }

    private fun Single<List<Rocket>>.getLoadRocketDisposableConfiguration(isForRefreshing: Boolean): Disposable {
        return subscribeOn(schedulers.getBackgroundThread())
            .observeOn(schedulers.getUiThread())
            .doOnSubscribe { viewState.value = getViewStateForOnSubscribe(isForRefreshing) }
            .subscribe(::handleGetRocketsSuccess, ::handleGetRocketsError)
    }

    private fun getViewStateForOnSubscribe(isForRefreshing: Boolean) =
        if (isForRefreshing) {
            ViewState.Refreshing
        } else {
            ViewState.Busy
        }

}
