package com.spacexapp.presentation.welcome

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.spacexapp.R

class WelcomeDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog =
        with(requireActivity()) {
            AlertDialog.Builder(this)
                .setView(layoutInflater.inflate(R.layout.dialog_fragment_welcome, null))
                .setNeutralButton(getString(R.string.positive_button_label)) { _, _ ->
                    (requireActivity() as? WelcomeDialogListener)?.onContinueClicked()
                        ?: throw ClassCastException()
                    dismiss()
                }
                .create()
        }

    companion object {
        const val TAG = "InstanceTag:WelcomeDialogFragment"
    }

    interface WelcomeDialogListener {
        fun onContinueClicked()
    }

}
