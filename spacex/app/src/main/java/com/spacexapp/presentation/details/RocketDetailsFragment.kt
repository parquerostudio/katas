package com.spacexapp.presentation.details

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.spacexapp.R
import com.spacexapp.app.di.ViewModelFactory
import com.spacexapp.domain.model.Rocket
import com.spacexapp.presentation.details.adapter.RocketDetailsAdapter
import com.spacexapp.presentation.details.adapter.RocketDetailsListItem
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_rocket_details.rocket_details_content
import javax.inject.Inject

class RocketDetailsFragment : Fragment() {

    @Inject lateinit var viewModelFactory: ViewModelFactory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(RocketDetailsViewModel::class.java)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewState()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_rocket_details, container, false).also {
        arguments?.getParcelable<Rocket>(ROCKET_TO_SHOW)?.let { rocket ->
            viewModel.loadRocketDetails(rocket)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRocketDetails()
    }

    private fun setupRocketDetails() {
        with(rocket_details_content) {
            adapter = RocketDetailsAdapter()
        }
    }

    private fun observeViewState() {
        viewModel.getViewState()
            .observe(viewLifecycleOwner, Observer<DetailsViewState> { viewState ->
                when (viewState) {
                    is DetailsViewState.Done -> showRocketDetails(viewState.rocketDetails)
                }
            })
    }

    private fun showRocketDetails(details: List<RocketDetailsListItem>) {
        (rocket_details_content.adapter as RocketDetailsAdapter).submitList(details)
    }

    companion object {

        private const val ROCKET_TO_SHOW = "Key:rocket_to_show"

        fun newInstance(rocket: Rocket) =
            RocketDetailsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ROCKET_TO_SHOW, rocket)
                }
            }
    }

}