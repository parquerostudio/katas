package com.spacexapp.presentation.details.adapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import com.spacexapp.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list_details_launch_information.view.launch_date
import kotlinx.android.synthetic.main.item_list_details_launch_information.view.launch_details_container
import kotlinx.android.synthetic.main.item_list_details_launch_information.view.launch_image
import kotlinx.android.synthetic.main.item_list_details_launch_information.view.mission_name
import kotlinx.android.synthetic.main.item_list_details_launches_graphic.view.total_launches
import kotlinx.android.synthetic.main.item_list_details_launches_header.view.year_of_launch_label
import kotlinx.android.synthetic.main.item_list_details_rocket_description.view.rocket_description_label
import java.text.SimpleDateFormat
import java.util.Date

sealed class RocketDetailsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    abstract fun bind(rocketDetailsItem: RocketDetailsListItem)

    class GraphicViewHolder(view: View) : RocketDetailsViewHolder(view) {

        override fun bind(rocketDetailsItem: RocketDetailsListItem) {
            val launchesPerYearData =
                (rocketDetailsItem as? RocketDetailsListItem.LaunchesGraphic)
                    ?: throw IllegalStateException("This view holder must be bind by a launch graphic item")


            // TODO implement the chart view and remove this
            var count = 0
            launchesPerYearData.launchesPerYear.forEach {
                count += it.second
            }
            itemView.total_launches.text =
                "Total launches: $count Years with launches: ${launchesPerYearData.launchesPerYear.size}"
        }
    }

    class DescriptionViewHolder(view: View) : RocketDetailsViewHolder(view) {

        override fun bind(rocketDetailsItem: RocketDetailsListItem) {
            val rocketDescription =
                (rocketDetailsItem as? RocketDetailsListItem.RocketDescription)?.description
                    ?: throw IllegalStateException("This view holder must be bind by a rocket description item")

            itemView.rocket_description_label.text = rocketDescription
        }
    }

    class LaunchHeaderViewHolder(view: View) : RocketDetailsViewHolder(view) {

        override fun bind(rocketDetailsItem: RocketDetailsListItem) {
            val yearOfLaunch =
                (rocketDetailsItem as? RocketDetailsListItem.RocketLaunches.YearOfLaunch)?.year
                    ?: throw IllegalStateException("This view holder must be bind by a year of launch item")

            itemView.year_of_launch_label.text = yearOfLaunch.toString()
        }
    }

    class LaunchItemViewHolder(view: View) : RocketDetailsViewHolder(view) {

        override fun bind(rocketDetailsItem: RocketDetailsListItem) {
            val launch =
                (rocketDetailsItem as? RocketDetailsListItem.RocketLaunches.Launch)
                    ?: throw IllegalStateException("This view holder must be bind by a rocket launch item")

            with(itemView) {
                launch_details_container.setBackgroundColor(
                    ContextCompat.getColor(context, getBackgroundColor(launch.wasSuccessful))
                )

                mission_name.text = launch.missionName
                launch_date.text = launch.date.getDateToShow()
                launch.imageUrl?.let {
                    Picasso.with(itemView.context)
                        .load(it).fit().centerCrop()
                        .placeholder(android.R.drawable.ic_dialog_alert)
                        .into(launch_image)
                }
            }
        }

        private fun getBackgroundColor(wasSuccessful: Boolean?) =
            when (wasSuccessful) {
                true -> R.color.successful_launch_color
                false -> R.color.fail_launch_color
                else -> R.color.not_launch_yet_color
            }
    }

}

private fun Date.getDateToShow(): String {
    val format = SimpleDateFormat("dd/MMM/yyyy")
    return format.format(this)
}
