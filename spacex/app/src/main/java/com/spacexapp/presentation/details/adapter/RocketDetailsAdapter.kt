package com.spacexapp.presentation.details.adapter

import android.support.v7.recyclerview.extensions.ListAdapter
import android.view.ViewGroup
import com.spacexapp.R
import com.spacexapp.app.utils.inflate

private const val LAUNCHES_GRAPHIC = 0
private const val DESCRIPTION = 1
private const val LAUNCH_YEAR_HEADER = 2
private const val LAUNCH_INFORMATION_ITEM = 3

class RocketDetailsAdapter :
    ListAdapter<RocketDetailsListItem, RocketDetailsViewHolder>(RocketDetailsDiffItemCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        when (viewType) {
            LAUNCHES_GRAPHIC -> RocketDetailsViewHolder.GraphicViewHolder(parent.inflate(R.layout.item_list_details_launches_graphic))
            DESCRIPTION -> RocketDetailsViewHolder.DescriptionViewHolder(parent.inflate(R.layout.item_list_details_rocket_description))
            LAUNCH_YEAR_HEADER -> RocketDetailsViewHolder.LaunchHeaderViewHolder(parent.inflate(R.layout.item_list_details_launches_header))
            LAUNCH_INFORMATION_ITEM -> RocketDetailsViewHolder.LaunchItemViewHolder(parent.inflate(R.layout.item_list_details_launch_information))
            else -> throw IllegalArgumentException("It should be a RocketDetailsListItem item")
        }

    override fun onBindViewHolder(holder: RocketDetailsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int) =
        when (getItem(position)) {
            is RocketDetailsListItem.LaunchesGraphic -> LAUNCHES_GRAPHIC
            is RocketDetailsListItem.RocketDescription -> DESCRIPTION
            is RocketDetailsListItem.RocketLaunches.YearOfLaunch -> LAUNCH_YEAR_HEADER
            is RocketDetailsListItem.RocketLaunches.Launch -> LAUNCH_INFORMATION_ITEM
        }

}
