package com.spacexapp.presentation.navigation

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.spacexapp.app.utils.SingleLiveEvent
import com.spacexapp.domain.model.Rocket

class SpaceXNavigator(
    val navigateToDetailsTrigger: MutableLiveData<Rocket> = SingleLiveEvent()
) {

    fun triggerNavigationToDetails(rocket: Rocket) {
        navigateToDetailsTrigger.postValue(rocket)
    }
}

fun <T> LiveData<T>.reObserve(owner: LifecycleOwner, observer: Observer<T>) {
    removeObserver(observer)
    observe(owner, observer)
}