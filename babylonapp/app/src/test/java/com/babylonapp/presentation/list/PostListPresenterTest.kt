package com.babylonapp.presentation.list

import com.babylonapp.MockSchedulers
import com.babylonapp.app.utils.ErrorHandler
import com.babylonapp.domain.PostsRepository
import com.babylonapp.domain.model.Post
import com.babylonapp.presentation.list.adapter.PostListItem
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class PostListPresenterTest {

    private val repository = mock<PostsRepository>()
    private val errorHandler = mock<ErrorHandler>()
    private val schedulers = MockSchedulers
    private val view = mock<PostListPresenter.PostListView>()

    private val presenter = PostListPresenter(schedulers, errorHandler, repository)
    private val samplePost = Post(1, 1, "title", "body")

    @Before
    fun setUp() {
        presenter.attachView(view)
    }

    @After
    fun tearDown() {
        verifyNoMoreInteractions(errorHandler, repository, view)
        presenter.detachView()
    }

    @Test
    fun `on onViewInitialized shows error when repository throws an error`() {
        // Given
        val errorMessage = "Some error message"
        val error = Throwable(errorMessage)
        whenever(repository.getPosts()).thenReturn(Single.error(error))

        // When
        presenter.onViewInitialized()

        // Then
        verify(view, times(1)).showLoading()
        verify(repository, times(1)).getPosts()
        verify(view, times(1)).hideLoading()
        verify(errorHandler, times(1)).handleNetworkResponseError(any(), eq(error))
        verify(view, times(1)).displayError(errorMessage)
    }

    @Test
    fun `on onViewInitialized normalizes list of posts and display them when received a post list`() {
        // Given
        val postTitle1 = "title 1"
        val postTitle2 = "title 2"
        val postTitle3 = "title 3"
        val postList = listOf(
            samplePost.copy(title = postTitle1),
            samplePost.copy(title = postTitle2),
            samplePost.copy(title = postTitle3)
        )
        whenever(repository.getPosts()).thenReturn(Single.just(postList))

        // When
        presenter.onViewInitialized()

        // Then
        verify(view, times(1)).showLoading()
        verify(repository, times(1)).getPosts()
        verify(view, times(1)).hideLoading()

        argumentCaptor<List<PostListItem>>().apply {
            verify(view, times(1)).displayPosts(capture())
            assertEquals(4, firstValue.size)    // Header + 3 posts
            assertTrue(firstValue[0] is PostListItem.Header)
            assertEquals(postTitle1, (firstValue[1] as PostListItem.PostItem).post.title)
            assertEquals(postTitle2, (firstValue[2] as PostListItem.PostItem).post.title)
            assertEquals(postTitle3, (firstValue[3] as PostListItem.PostItem).post.title)
        }
    }

}
