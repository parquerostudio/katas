package com.babylonapp.app.utils

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class Schedulers
@Inject constructor() : TasksSchedulers {

    private val backgroundThreadInstance: Scheduler by lazy { io.reactivex.schedulers.Schedulers.io() }

    private val uiThreadInstance: Scheduler by lazy { AndroidSchedulers.mainThread() }

    override fun getBackgroundThread(): Scheduler = backgroundThreadInstance

    override fun getUiThread(): Scheduler = uiThreadInstance

}
