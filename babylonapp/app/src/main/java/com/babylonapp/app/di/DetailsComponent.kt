package com.babylonapp.app.di

import com.babylonapp.presentation.details.DetailsFragment
import com.babylonapp.presentation.details.DetailsModule
import dagger.Subcomponent

@Subcomponent(
    modules = [
        DetailsModule::class
    ]
)
interface DetailsComponent {
    fun inject(fragment: DetailsFragment)
}
