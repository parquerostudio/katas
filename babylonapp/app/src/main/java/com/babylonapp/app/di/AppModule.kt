package com.babylonapp.app.di

import com.babylonapp.app.utils.ErrorHandler
import com.babylonapp.app.utils.Schedulers
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideRxSchedulers(): Schedulers = Schedulers()

    @Provides
    @Singleton
    fun provideErrorHandler(): ErrorHandler = ErrorHandler

}
