package com.babylonapp.app.di

import com.babylonapp.app.utils.ServiceGenerator
import com.babylonapp.data.PostsRepositoryImp
import com.babylonapp.data.local.PostsMemoryDataSourceImp
import com.babylonapp.data.network.PostNetworkParser
import com.babylonapp.data.network.PostsApiClient
import com.babylonapp.data.network.PostsNetworkDataSourceImp
import com.babylonapp.domain.PostsMemoryDataSource
import com.babylonapp.domain.PostsNetworkDataSource
import com.babylonapp.domain.PostsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providePostsNetworkDataSource(): PostsNetworkDataSource =
        PostsNetworkDataSourceImp(
            ServiceGenerator.createService(PostsApiClient::class.java),
            PostNetworkParser
        )

    @Provides
    @Singleton
    fun providePostsMemoryDataSource(): PostsMemoryDataSource = PostsMemoryDataSourceImp()

    @Provides
    @Singleton
    fun providePostsRepository(
        networkDataSource: PostsNetworkDataSource,
        memoryDataSource: PostsMemoryDataSource
    ): PostsRepository =
        PostsRepositoryImp(networkDataSource, memoryDataSource)

}
