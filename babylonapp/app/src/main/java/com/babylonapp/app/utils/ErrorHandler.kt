package com.babylonapp.app.utils

import android.util.Log

object ErrorHandler {

    fun handleViewNullError(tag: String) {
        handleError(tag, Throwable("$tag: View not available"))
    }

    fun handleNetworkResponseError(tag: String, error: Throwable) {
        handleError(tag, error)
    }

    private fun handleError(tag: String, error: Throwable) {
        // TODO send error through Fabric or any other log tool


        // This should be only in the debug flavor
        Log.d(tag, "$tag: ${error.message}")
    }

}