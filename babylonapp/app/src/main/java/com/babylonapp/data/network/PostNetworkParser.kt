package com.babylonapp.data.network

import com.babylonapp.data.network.model.NetworkComment
import com.babylonapp.data.network.model.NetworkPost
import com.babylonapp.data.network.model.NetworkUser
import com.babylonapp.domain.model.Comment
import com.babylonapp.domain.model.Post
import com.babylonapp.domain.model.User

/**
 * Imagine the classes we receive are not the same than what we need. We can transform from data model
 * to domain model in this parser.
 */
object PostNetworkParser {

    fun parsePostsResponse(postResponse: List<NetworkPost>): List<Post> {
        return postResponse.map { it.toDomainModel() }
    }

    fun parseUsersResponse(usersResponse: List<NetworkUser>): List<User> {
        return usersResponse.map { it.toDomainModel() }
    }

    fun parseCommentsResponse(commentsResponse: List<NetworkComment>): List<Comment> {
        return commentsResponse.map { it.toDomainModel() }
    }

    private fun NetworkPost.toDomainModel() = Post(id, userId, title, body)

    private fun NetworkUser.toDomainModel() = User(id, name, userName)

    private fun NetworkComment.toDomainModel() = Comment(id, postId, name, body)

}
