package com.babylonapp.data.network

import com.babylonapp.data.network.model.NetworkComment
import com.babylonapp.data.network.model.NetworkPost
import com.babylonapp.data.network.model.NetworkUser
import com.babylonapp.domain.PostsNetworkDataSource
import com.babylonapp.domain.model.Comment
import com.babylonapp.domain.model.Post
import com.babylonapp.domain.model.User
import io.reactivex.Single
import java.io.IOException
import javax.inject.Inject

class PostsNetworkDataSourceImp
@Inject constructor(
    private val apiService: PostsApiClient,
    private val parser: PostNetworkParser
) : PostsNetworkDataSource {

    override fun getPosts(): Single<List<Post>> {
        return Single.create<List<NetworkPost>> { emitter ->
            try {
                apiService.getPosts().execute().body()?.let { response ->
                    emitter.onSuccess(response)
                }
            } catch (e: IOException) {
                emitter.onError(Throwable(e.message))
            }
            if (!emitter.isDisposed) {
                emitter.onError(Throwable("We had an issue downloading posts"))
            }
        }.map { networkResponse ->
            parser.parsePostsResponse(networkResponse)
        }
    }

    override fun getUsers(): Single<List<User>> {
        return Single.create<List<NetworkUser>> { emitter ->
            try {
                apiService.getUsers().execute().body()?.let { response ->
                    emitter.onSuccess(response)
                }
            } catch (e: IOException) {
                emitter.onError(Throwable(e.message))
            }
            if (!emitter.isDisposed) {
                emitter.onError(Throwable("We had an issue downloading users"))
            }
        }.map { networkResponse ->
            parser.parseUsersResponse(networkResponse)
        }
    }

    override fun getComments(): Single<List<Comment>> {
        return Single.create<List<NetworkComment>> { emitter ->
            try {
                apiService.getComments().execute().body()?.let { response ->
                    emitter.onSuccess(response)
                }
            } catch (e: IOException) {
                emitter.onError(Throwable(e.message))
            }
            if (!emitter.isDisposed) {
                emitter.onError(Throwable("We had an issue downloading comments"))
            }
        }.map { networkResponse ->
            parser.parseCommentsResponse(networkResponse)
        }
    }

}
