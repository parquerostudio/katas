package com.babylonapp.data.network.model

import com.google.gson.annotations.SerializedName

data class NetworkPost(
    @SerializedName("id") val id: Int,
    @SerializedName("userId") val userId: Int,
    @SerializedName("title") val title: String,
    @SerializedName("body") val body: String
)
