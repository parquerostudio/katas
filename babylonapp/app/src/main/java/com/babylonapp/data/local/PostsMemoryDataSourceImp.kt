package com.babylonapp.data.local

import com.babylonapp.domain.PostsMemoryDataSource
import com.babylonapp.domain.model.Comment
import com.babylonapp.domain.model.Post
import com.babylonapp.domain.model.User
import io.reactivex.Single
import javax.inject.Inject

class PostsMemoryDataSourceImp
@Inject constructor() : PostsMemoryDataSource {

    private val postsCache = mutableListOf<Post>()
    private val usersCache = mutableListOf<User>()
    private val commentsCache = mutableListOf<Comment>()

    override fun getPosts() = Single.just(postsCache.toList())

    override fun cachePosts(posts: List<Post>) {
        postsCache.clear()
        postsCache.addAll(posts)
    }

    override fun getUsers() = Single.just(usersCache.toList())

    override fun cacheUsers(users: List<User>) {
        usersCache.clear()
        usersCache.addAll(users)
    }

    override fun getComments() = Single.just(commentsCache.toList())

    override fun cacheComments(comments: List<Comment>) {
        commentsCache.clear()
        commentsCache.addAll(comments)
    }

}
