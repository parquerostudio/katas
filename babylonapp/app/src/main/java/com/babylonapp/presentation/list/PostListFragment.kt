package com.babylonapp.presentation.list

import android.os.Bundle
import android.view.View
import com.babylonapp.R
import com.babylonapp.app.BabylonApplication
import com.babylonapp.domain.model.Post
import com.babylonapp.presentation.base.BaseFragment
import com.babylonapp.presentation.list.adapter.PostAdapterListener
import com.babylonapp.presentation.list.adapter.PostListAdapter
import com.babylonapp.presentation.list.adapter.PostListItem
import kotlinx.android.synthetic.main.fragment_list.post_list_view
import javax.inject.Inject

class PostListFragment :
    BaseFragment<PostListFragment.Callback, PostListPresenter.PostListView, PostListPresenter>(),
    PostListPresenter.PostListView,
    PostAdapterListener {

    @Inject
    lateinit var presenterInstance: PostListPresenter

    // region BaseFragment functions
    override val layoutResourceId: Int
        get() = R.layout.fragment_list

    override fun getPresenter() = presenterInstance

    override fun getMVPViewReference() = this

    override fun initializeInjector() {
        (requireActivity().application as BabylonApplication)
            .component
            .postListComponent(PostListModule())
            .inject(this)
    }
    // endregion

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupPostList()
        presenterInstance.onViewInitialized()
    }

    private fun setupPostList() {
        post_list_view.adapter = PostListAdapter(this)
    }

    // region PostListView functions
    override fun showLoading() {
        callback?.onShowLoading()
    }

    override fun hideLoading() {
        callback?.onHideLoading()
    }

    override fun displayPosts(dataToShow: List<PostListItem>) {
        (post_list_view.adapter as? PostListAdapter)?.submitList(dataToShow)
    }

    override fun displayError(error: String) {
        callback?.onDisplayError(error)
    }
    // endregion

    override fun onPostClicked(post: Post) {
        callback?.onPostClicked(post)
    }

    interface Callback {

        fun onShowLoading()

        fun onHideLoading()

        fun onPostClicked(post: Post)

        fun onDisplayError(errorMessage: String)

    }

    companion object {

        fun newInstance() =
            PostListFragment().apply {
                arguments = Bundle()
            }

    }
}
