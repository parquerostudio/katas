package com.babylonapp.presentation.list.adapter

import android.support.v7.recyclerview.extensions.ListAdapter
import android.view.ViewGroup
import com.babylonapp.R
import com.babylonapp.presentation.inflate

private const val HEADER = 0
private const val POST = 1

class PostListAdapter(
    private val postItemClickListener: PostAdapterListener
) : ListAdapter<PostListItem, PostListViewHolder>(PostItemDiffItemCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        when (viewType) {
            HEADER -> PostListViewHolder.HeaderViewHolder(parent.inflate(R.layout.list_item_header))
            else -> PostListViewHolder.PostItemViewHolder(parent.inflate(R.layout.list_item_post))
        }

    override fun onBindViewHolder(holder: PostListViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener {
            postItemClickListener.onPostClicked((getItem(position) as PostListItem.PostItem).post)
        }
    }

    override fun getItemViewType(position: Int) =
        when (getItem(position)) {
            is PostListItem.Header -> HEADER
            is PostListItem.PostItem -> POST
        }

}
