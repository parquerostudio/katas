package com.babylonapp.presentation.list.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.babylonapp.R
import kotlinx.android.synthetic.main.list_item_header.view.header_label
import kotlinx.android.synthetic.main.list_item_post.view.title

sealed class PostListViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    abstract fun bind(postListItem: PostListItem)

    class HeaderViewHolder(view: View) : PostListViewHolder(view) {

        override fun bind(postListItem: PostListItem) {
            val header = postListItem as? PostListItem.Header
                ?: throw IllegalStateException("This view holder must be bind by a header item")

            with(itemView) {
                header_label.text = resources.getString(R.string.post_list_header)
            }
        }
    }

    class PostItemViewHolder(view: View) : PostListViewHolder(view) {

        override fun bind(postListItem: PostListItem) {
            val postItem = postListItem as? PostListItem.PostItem
                ?: throw IllegalStateException("This view holder must be bind by a post item")

            with(itemView) {
                title.text = postItem.post.title
            }
        }
    }

}
