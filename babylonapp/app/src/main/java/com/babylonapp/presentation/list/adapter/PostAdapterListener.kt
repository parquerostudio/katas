package com.babylonapp.presentation.list.adapter

import com.babylonapp.domain.model.Post

interface PostAdapterListener {
    fun onPostClicked(post: Post)
}
