package com.babylonapp.presentation.list

import com.babylonapp.app.utils.ErrorHandler
import com.babylonapp.app.utils.TasksSchedulers
import com.babylonapp.domain.PostsRepository
import com.babylonapp.domain.model.Post
import com.babylonapp.presentation.base.BasePresenter
import com.babylonapp.presentation.base.BasePresenterImp
import com.babylonapp.presentation.list.adapter.PostListItem
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

class PostListPresenter
@Inject constructor(
    private val schedulers: TasksSchedulers,
    override val errorHandler: ErrorHandler,
    private val repository: PostsRepository
) : BasePresenterImp<PostListPresenter.PostListView>(errorHandler) {

    fun onViewInitialized() {
        disposables += repository.getPosts()
            .subscribeOn(schedulers.getBackgroundThread())
            .observeOn(schedulers.getUiThread())
            .doOnSubscribe { getView()?.showLoading() }
            .doAfterTerminate { getView()?.hideLoading() }
            .subscribe(::handleSuccessResult, ::handleErrorResult)
    }

    private fun handleSuccessResult(posts: List<Post>) {
        getView()?.displayPosts(normalizePostsToShow(posts))
    }

    private fun handleErrorResult(error: Throwable) {
        errorHandler.handleNetworkResponseError(javaClass.simpleName, error)
        getView()?.displayError(error.message ?: "")
    }

    private fun normalizePostsToShow(posts: List<Post>): List<PostListItem> {
        return listOf(PostListItem.Header) +
            createPostListItems(posts)
    }

    private fun createPostListItems(posts: List<Post>) =
        posts.map {
            PostListItem.PostItem(it)
        }

    interface PostListView : BasePresenter.BaseView {

        fun showLoading()

        fun hideLoading()

        fun displayPosts(dataToShow: List<PostListItem>)

        fun displayError(error: String)

    }

}
