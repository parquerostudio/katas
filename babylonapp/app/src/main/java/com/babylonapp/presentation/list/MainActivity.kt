package com.babylonapp.presentation.list

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.babylonapp.R
import com.babylonapp.app.BabylonApplication
import com.babylonapp.domain.model.Post
import com.babylonapp.presentation.base.BaseActivity
import com.babylonapp.presentation.details.DetailsFragment
import javax.inject.Inject

private const val POST_LIST_SCREEN_TAG = "Fragment:PostListFragment"
private const val POST_DETAILS_SCREEN_TAG = "Fragment:DetailsFragment"

class MainActivity : BaseActivity<MainActivityPresenter.MainActivityView, MainActivityPresenter>(),
    MainActivityPresenter.MainActivityView,
    PostListFragment.Callback,
    DetailsFragment.Callback {

    @Inject
    lateinit var presenterInstance: MainActivityPresenter

    // region BaseActivity functions
    override fun getPresenter(): MainActivityPresenter = presenterInstance

    override fun getMVPViewReference(): MainActivityPresenter.MainActivityView = this

    override fun initializeInjector() {
        (application as BabylonApplication).component
            .mainActivityComponent(MainActivityModule())
            .inject(this)
    }
    // endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showPostListFragment()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
            else -> {
                // Do nothing
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        super.onBackPressed()
    }

    // region MainActivityView functions
    override fun showLoading() {
        // TODO implement show loading
        Toast.makeText(this, "Loading", Toast.LENGTH_SHORT).show()
    }

    override fun hideLoading() {
        // TODO implement hide loading
        Toast.makeText(this, "No-Loading", Toast.LENGTH_SHORT).show()
    }
    // endregion

    // region PostListFragment.Callback functions
    override fun onShowLoading() {
        getPresenter().onShowLoadingFromPostListFragment()
    }

    override fun onHideLoading() {
        getPresenter().onHideLoadingFromPostListFragment()
    }

    override fun onPostClicked(post: Post) {
        showDetailsFragment(post)
    }
    // endregion

    override fun onDisplayError(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
    }

    private fun showPostListFragment() {
        val postListFragment =
            supportFragmentManager.findFragmentByTag(POST_LIST_SCREEN_TAG) as? PostListFragment
        if (postListFragment == null) {
            val newPostListFragment = PostListFragment.newInstance()
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, newPostListFragment, POST_LIST_SCREEN_TAG)
                .commit()
        }
    }

    private fun showDetailsFragment(post: Post) {
        val detailsFragment =
            supportFragmentManager.findFragmentByTag(POST_DETAILS_SCREEN_TAG) as? DetailsFragment
        if (detailsFragment == null) {
            val newDetailsFragment = DetailsFragment.newInstance(post)
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, newDetailsFragment, POST_DETAILS_SCREEN_TAG)
                .addToBackStack(POST_DETAILS_SCREEN_TAG)
                .commit()
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}
