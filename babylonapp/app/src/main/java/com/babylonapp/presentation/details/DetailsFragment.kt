package com.babylonapp.presentation.details


import android.os.Bundle
import android.view.View
import com.babylonapp.R
import com.babylonapp.app.BabylonApplication
import com.babylonapp.domain.model.Post
import com.babylonapp.presentation.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_details.post_body
import kotlinx.android.synthetic.main.fragment_details.post_comments_counter
import kotlinx.android.synthetic.main.fragment_details.post_title
import kotlinx.android.synthetic.main.fragment_details.user_name
import javax.inject.Inject

class DetailsFragment :
    BaseFragment<DetailsFragment.Callback, DetailsPresenter.DetailsView, DetailsPresenter>(),
    DetailsPresenter.DetailsView {

    @Inject
    lateinit var presenterInstance: DetailsPresenter

    // region BaseFragment functions
    override val layoutResourceId: Int
        get() = R.layout.fragment_details

    override fun getPresenter() = presenterInstance

    override fun getMVPViewReference() = this

    override fun initializeInjector() {
        (requireActivity().application as BabylonApplication)
            .component
            .detailsComponent(DetailsModule())
            .inject(this)
    }
    // endregion

    // region DetailsView functions
    override fun showLoading() {
        callback?.onShowLoading()
    }

    override fun hideLoading() {
        callback?.onHideLoading()
    }

    override fun showTitle(title: String) {
        post_title.text = title
    }

    override fun showBody(body: String) {
        post_body.text = body
    }

    override fun displayOtherDetails(userName: String?, commentCount: Int) {
        user_name.text = userName ?: ""

        post_comments_counter.text = resources.getQuantityString(R.plurals.comments_counter, commentCount, commentCount)
    }

    override fun displayError(s: String) {
        callback?.onDisplayError(s)
    }
    // endregion

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getParcelable<Post>(POST_TO_SHOW)?.let {
            presenterInstance.onViewInitialized(it)
        }
    }

    interface Callback {
        fun onShowLoading()

        fun onHideLoading()

        fun onDisplayError(errorMessage: String)
    }

    companion object {

        private const val POST_TO_SHOW = "Key:post_to_show"

        fun newInstance(post: Post) =
            DetailsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(POST_TO_SHOW, post)
                }
            }
    }

}
