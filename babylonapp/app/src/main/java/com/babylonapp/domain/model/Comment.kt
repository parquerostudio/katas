package com.babylonapp.domain.model

data class Comment(
    val id: Int,
    val postId: Int,
    val name: String,
    val content: String
)
