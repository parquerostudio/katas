package com.babylonapp.domain

import com.babylonapp.domain.model.Comment
import com.babylonapp.domain.model.Post
import com.babylonapp.domain.model.User
import io.reactivex.Single

interface PostsMemoryDataSource {

    fun getPosts(): Single<List<Post>>

    fun cachePosts(posts: List<Post>)

    fun getUsers(): Single<List<User>>

    fun cacheUsers(users: List<User>)

    fun getComments(): Single<List<Comment>>

    fun cacheComments(comments: List<Comment>)

}
